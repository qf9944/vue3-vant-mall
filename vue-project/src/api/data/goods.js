export function goodsForHome(_len=10) {
  //console.log('allGoods.length:',allGoods.length);
  if(!goodsGData.allGoods){
  	  return [];
  }
  //console.log('goodsForHome:',goodsGData.allGoods);
  let len= Math.min(_len,goodsGData.allGoods.length);
  let goods=[];
  let item;
  for(let i=0;i<len;i++){
    item=goodsGData.allGoods[i];
    goods.push({
      id: item.skuList[0].skuId,
      title: item.title,
      image: item.primaryImage,
      price: parseInt(item.minSalePrice)*0.01,
      delPrice: parseInt(item.maxLinePrice)*0.01,
      tag: item.spuTagList[0]?.title,
    });
  }
  item=null;
  return goods;
}
//
export function goodsForCart(_len=6) {
  //console.log('allGoods.length:',allGoods.length);
  if(!goodsGData.allGoods){
  	  return [];
  }
  let len= Math.min(_len,goodsGData.allGoods.length);
  let goods=[];
  let item,i,j;
  for(let i=0;i<len;i++){
    item=goodsGData.allGoods[i];
    let specValueStr='';
    for(j=0;j<item.specList.length;j++){
      let _rand=parseInt(item.specList[j].specValueList.length*Math.random());
      if(specValueStr=='')
        specValueStr+=item.specList[j].specValueList[_rand].specValue;
      else
        specValueStr+=' + '+item.specList[j].specValueList[_rand].specValue;
    }
    specValueStr+=' ';
    //console.log('specValueStr:',specValueStr);
    let numStock=(i==len-1?0:len-i);
    goods.push({
      id: item.skuList[0].skuId,
      title: item.title,
      image: item.primaryImage,
      price: parseInt(item.minSalePrice)*0.01,
      spec: specValueStr,
      selected:numStock>0,
      numBuys:i==len-1?0:Math.min(1,numStock),
      numStock:numStock
    });
  }
  item=null;
  return goods;
}
export function goodsForOrder(_len=2) {
  //console.log('allGoods.length:',allGoods.length);
  if(!goodsGData.allGoods){
  	  return [];
  }
  let _allGoods=goodsGData.allGoods.sort((a,b)=>Math.random()>0.5?1:-1);
  let len= Math.min(_len,_allGoods.length);
  let goods=[];
  let item,i,j;
  for(let i=0;i<len;i++){
    item=_allGoods[i];
    let specValueStr='';
    for(j=0;j<item.specList.length;j++){
      let _rand=parseInt(item.specList[j].specValueList.length*Math.random());
      if(specValueStr=='')
        specValueStr+=item.specList[j].specValueList[_rand].specValue;
      else
        specValueStr+=' + '+item.specList[j].specValueList[_rand].specValue;
    }
    specValueStr+=' ';
    goods.push({
      id: item.skuList[0].skuId,
      title: item.title,
      image: item.primaryImage,
      price: parseInt(item.minSalePrice)*0.01,
      spec: specValueStr,
      numBuys:(Math.random()>0.7?(Math.random()>0.7?3:2):1),
    });
  }
  item=null;
  return goods;
}
//
export function goodsDetail(id) {
	if(!goodsGData.allGoods){
		  return [];
	}
  let item = goodsGData.allGoods.filter(item=>item.skuList[0].skuId==id)[0];
  //
  let i,j;
  let specList=[];
  for(i=0;i<item.specList.length;i++){
    let valueList=[];
    for(j=0;j<item.specList[i].specValueList.length;j++){
      valueList.push({value:item.specList[i].specValueList[j].specValue,enable:true});
    }
    let currentIndex=parseInt(Math.random()*valueList.length);
    if(valueList.length>1)currentIndex=-1;
    let disabledIndex=parseInt(Math.random()*valueList.length);
    if(currentIndex!=disabledIndex && Math.random()>0.8){
      valueList[disabledIndex].enable=false;
    }
    specList.push({
      title:item.specList[i].title,
      list:valueList,
      current:currentIndex
    });
  }
  //
  let data = {
    id: item.skuList[0].skuId,
    title: item.title,
    images: item.images,
    price: parseInt(item.minSalePrice)*0.01,
    delPrice: parseInt(item.maxLinePrice)*0.01,
    soldNum: item.soldNum,
    descImages: item.desc,
    image: item.primaryImage,
    specs: specList,
    numBuys:0,
    spec:'',
    selected:true,
    numStock:5
  }
  return data;
}
export const goodsGData={
	/*这里export后的变量不能被重新赋值，跟const无关，只能从内部赋值*/
	allGoods:JSON.parse(localStorage.getItem('allGoods')),
	goodsForCommit:null,
	goodsDetailByEdited:null,
	goodsForRefund:null
};
