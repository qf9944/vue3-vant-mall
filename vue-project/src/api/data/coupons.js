export const couponsList = [{
		name: '折扣券',
		limit: 200,
		discount: 9.8,
		selected: false,
		valid_date: '2024-01-12 23:59:59',
		disabled: false
	},
	{
		name: '季度福利卡',
		limit: 1000,
		discount: 9.5,
		selected: false,
		valid_date: '2024-02-12 12:00:00',
		disabled: false
	},
	{
		name: '周年庆折扣卡',
		limit: 2000,
		discount: 9.2,
		selected: false,
		valid_date: '2024-03-12 12:00:00',
		disabled: false
	},
	{
		name: '老会员福利卡',
		limit: 3000,
		discount: 9.0,
		selected: false,
		valid_date: '2024-04-12 00:00:00',
		disabled: false
	}
];
