//安装: npm install vuex@next --save
import { createStore } from 'vuex'
//
const store = createStore({
	state () {
		return {
			route: {}
		}
	},
	  //用来操作state，actions中调用commit方法来调用mutations
	mutations: {
		onRouteChange(state,payload) {
			//console.log('onRouteChange:',payload);
			state.route=payload;
		}
	},
  //全局异步操作，异步操作mutations方法，比如用于判断登录
	actions: {
		/*
		route_changed(context,payload){
			context.commit('onRouteChange',payload);
		}
		*/
	},
	//计算属性
	getters: {
		/*
		getCounter(state){
			return state.counter*10;
		}
		*/
	},
	//其他：当不需要对数据进行额外加工的时候，可以直接在组件中调用commit方法触发mutations中的方法
});
//
export default store

