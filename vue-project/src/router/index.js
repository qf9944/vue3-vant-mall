//安装使用: npm install vue-router@4
import {
	createRouter,
	createWebHashHistory
} from 'vue-router'
//
import Home from '../views/home.vue'
import Category from '../views/category.vue'
import Campaign from '../views/campaign.vue'
import Cart from '../views/cart/index.vue'
import CartBill from '../views/cart/bill.vue'
import CartCommit from '../views/cart/commit.vue'
import AddressList from '../views/address/list.vue'
import AddressEdit from '../views/address/edit.vue'
import GoodsDetail from '../views/goods-detail/index.vue'
import GoodsDetailComments from '../views/goods-detail/comments.vue'
import Search from '../views/search/index.vue'
import SearchResults from '../views/search/results.vue'
import UserCenter from '../views/usercenter/index.vue'
import Orders from '../views/usercenter/orders.vue'
import Refund from '../views/usercenter/refund-detail.vue'
import CommentEdit from '../views/usercenter/comment-edit.vue'
import PersonInfo from '../views/usercenter/person-info/index.vue'
import PersonNameEdit from '../views/usercenter/person-info/name-edit.vue'
import PersonPhoneEdit from '../views/usercenter/person-info/phone-edit.vue'
import AfterSelling from '../views/after-selling/index.vue'
import TrackingEdit from '../views/after-selling/tracking-edit.vue'
import TrackingInfo from '../views/after-selling/tracking-info.vue'
import Coupons from '../views/coupons.vue'
// 定义路由
const routes = [{
		path: '/home',
		name: '首页',
		component: Home,
		meta: {
			requireTapBar: true,
			hideTopBar: true,
			bgColor: '#f9f9f9'
		}
	},
	{
		path: '/category',
		name: '分类',
		component: Category,
		meta: {
			requireTapBar: true,
			hideTopBar: true
		}
	},
	{
		path: '/cart',
		name: '购物车',
		component: Cart,
		meta: {
			requireTapBar: true,
			hideTopBar: true,
			bgColor: '#f9f9f9'
		}
	},
	{
		path: '/usercenter',
		name: '个人中心',
		component: UserCenter,
		meta: {
			requireTapBar: true,
			hideTopBar: true
		}
	},
	{
		path: '/campaign',
		name: '优惠活动',
		component: Campaign
	},
	{
		path: '/cart-bill',
		name: '发票',
		component: CartBill
	},
	{
		path: '/cart-commit',
		name: '提交',
		component: CartCommit
	},
	{
		path: '/address-list',
		name: '收货地址列表',
		component: AddressList
	},
	{
		path: '/address-edit',
		name: '收货地址信息',
		component: AddressEdit
	},
	{
		path: '/goods-detail',
		name: '商品详情',
		component: GoodsDetail
	},
	{
		path: '/goods-detail-comments',
		name: '评论',
		component: GoodsDetailComments
	},
	{
		path: '/search',
		name: '搜索',
		component: Search
	},
	{
		path: '/search-results',
		name: '搜索结果',
		component: SearchResults
	},
	{
		path: '/orders',
		name: '订单列表',
		component: Orders
	},
	{
		path: '/refund',
		name: '退货退款',
		component: Refund
	},
	{
		path: '/comment-edit',
		name: '发表评论',
		component: CommentEdit
	},
	{
		path: '/person-info',
		name: '个人信息',
		component: PersonInfo
	},
	{
		path: '/person-name-edit',
		name: '编辑昵称',
		component: PersonNameEdit
	},
	{
		path: '/person-phone-edit',
		name: '编辑电话号码',
		component: PersonPhoneEdit
	},
	{
		path: '/after-selling',
		name: '售后',
		component: AfterSelling
	},
	{
		path: '/tracking-edit',
		name: '修改订单',
		component: TrackingEdit
	},
	{
		path: '/tracking-info',
		name: '物流详情',
		component: TrackingInfo
	},
	{
		path: '/coupons',
		name: '优惠券',
		component: Coupons
	}
];
// 创建路由器
const router = createRouter({
	history: createWebHashHistory(),
	routes: routes,
	scrollBehavior(to, from, saveScrollPosition) {
		// 跳转路由后滚动置顶
		return {
			left: 0,
			top: 0
		};
	}
})
export default router;
