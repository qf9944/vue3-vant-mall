import './assets/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//
//引入你需要的组件
import { showToast,Button,Icon,Field,Swipe,SwipeItem,Tab,Tabs,Search,Rate,Picker,Checkbox,CheckboxGroup,
RadioGroup,Radio,Cascader,Popup,Cell,CellGroup,Switch,Uploader,Loading,Overlay,ShareSheet,SwipeCell,Tag,Sidebar,
SidebarItem,Area,ContactCard,SubmitBar,Stepper,ActionBar,ActionBarIcon,ActionBarButton,showConfirmDialog,Steps,Step} from 'vant'
//引入组件样式 
import 'vant/lib/index.css'
//公共js
import { ref } from 'vue';
//
const app = createApp(App)
//配置公用方法，也可以直接用window.$ref
app.config.globalProperties.$ref = ref;
app.config.globalProperties.$showToast = showToast;
app.config.globalProperties.$showConfirmDialog = showConfirmDialog;
//注册全局组件名（不是必须）
app.component('van-button', Button)
.component('van-icon', Icon)
.component('van-field', Field)
.component('van-swipe', Swipe).component('van-swipe-item', SwipeItem)
.component('van-tab', Tab).component('van-tabs', Tabs)
.component('van-search', Search)
.component('van-rate', Rate)
.component('van-picker', Picker)
.component('van-checkbox', Checkbox)
.component('van-checkbox-group', CheckboxGroup)
.component('van-radio', Radio)
.component('van-radio-group', RadioGroup)
.component('van-cascader', Cascader)
.component('van-popup', Popup)
.component('van-cell', Cell).component('van-cell-group', CellGroup)
.component('van-switch', Switch)
.component('van-uploader', Uploader)
.component('van-loading', Loading)
.component('van-overlay', Overlay)
.component('van-share-sheet', ShareSheet)
.component('van-swipe-cell', SwipeCell)
.component('van-tag', Tag)
.component('van-sidebar', Sidebar).component('van-sidebar-item', SidebarItem)
.component('van-area', Area)
.component('van-contact-card', ContactCard)
.component('van-submit-bar', SubmitBar)
.component('van-stepper', Stepper)
.component('van-action-bar', ActionBar)
.component('van-action-bar-icon', ActionBarIcon)
.component('van-action-bar-button', ActionBarButton)
.component('van-steps', Steps)
.component('van-step', Step)
//整个应用支持路由
app.use(router)
//状态管理
.use(store)
//渲染
.mount('#app')
//
//导航守卫
router.beforeEach((to, from, next) => {
	//store.dispatch('route_changed',to.name); 用于store内部action异步处理
	store.commit('onRouteChange',to);//用于更新页面标题
	//如果有登录，这里再去判断token
	//如果页面不存在，则跳转至首页
	if(!to.name){
		router.push('/home');
		return;
	}
	next(); // 放行
})



