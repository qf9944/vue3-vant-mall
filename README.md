# vue3商城模版-vant版

#### 介绍
1.  h5移动端商城模版(vue3+vant)，设计流程参考腾讯TDesign模版，使用了腾讯官方素材;
2.  实现了基本的前端功能点，包括：组件封装(多插槽)、自定义tabbar与topbar、路由&守卫、vuex、axio封装、移动端自适应解决方案等；
3.  具有一定研究价值，亦可作为二次开发使用；
4.  观摩地址：http://42.194.249.219/dist
5.  5+App后的apk安装链接为：http://42.194.249.219/vue3-mall.apk
5.  原生小程序版本仓库地址为：https://gitee.com/qf9944/miniprogram-vant-mall
